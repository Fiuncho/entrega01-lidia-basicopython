#Bienvenida:
print("""\nWelcome to fdisk (util-linux 2.27.1).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.
""")

#Definicion de funciones que SOLO impriman texto:
def orden_M():
	print("""Help:

  DOS (MBR)
   a   toggle a bootable flag
   b   edit nested BSD disklabel
   c   toggle the dos compatibility flag

  Generic
   d   delete a partition
   F   list free unpartitioned space
   l   list known partition types
   n   add a new partition
   p   print the partition table
   t   change a partition type
   v   verify the partition table
   i   print information about a partition

  Misc
   m   print this menu
   u   change display/entry units
   x   extra functionality (experts only)

  Script
   I   load disk layout from sfdisk script file
   O   dump disk layout to sfdisk script file

  Save & Exit
   w   write table to disk and exit
   q   quit without saving changes

  Create a new label
   g   create a new empty GPT partition table
   G   create a new empty SGI (IRIX) partition table
   o   create a new empty DOS partition table
   s   create a new empty Sun partition table""") 

def orden_F():
	print("""Unpartitioned space /dev/sda: 9 GiB, 9665740288 bytes, 18878399 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes

   Start    Final Sectores Size
      63 18876413 18876351   9G
20969472 20971519     2048   1M""")

def orden_L():
	 print(""" 0  Vacía           24  DOS de NEC      81  Minix / Linux a bf  Solaris
 1  FAT12           27  WinRE NTFS ocul 82  Linux swap / So c1  DRDOS/sec (FAT-
 2  XENIX root      39  Plan 9          83  Linux           c4  DRDOS/sec (FAT-
 3  XENIX usr       3c  PartitionMagic  84  OS/2 hidden or  c6  DRDOS/sec (FAT-
 4  FAT16 <32M      40  Venix 80286     85  Linux extendida c7  Syrinx
 5  Extendida       41  PPC PReP Boot   86  Conjunto de vol da  Datos sin SF
 6  FAT16           42  SFS             87  Conjunto de vol db  CP/M / CTOS / .
 7  HPFS/NTFS/exFAT 4d  QNX4.x          88  Linux plaintext de  Utilidad Dell
 8  AIX             4e  QNX4.x segunda  8e  Linux LVM       df  BootIt
 9  AIX arrancable  4f  QNX4.x tercera  93  Amoeba          e1  DOS access
 a  Gestor de arran 50  OnTrack DM      94  Amoeba BBT      e3  DOS R/O
 b  W95 FAT32       51  OnTrack DM6 Aux 9f  BSD/OS          e4  SpeedStor
 c  W95 FAT32 (LBA) 52  CP/M            a0  Hibernación de  ea  Rufus alignment
 e  W95 FAT16 (LBA) 53  OnTrack DM6 Aux a5  FreeBSD         eb  BeOS fs
 f  W95 Ext'd (LBA) 54  OnTrackDM6      a6  OpenBSD         ee  GPT
10  OPUS            55  EZ-Drive        a7  NeXTSTEP        ef  EFI (FAT-12/16/
11  FAT12 oculta    56  Golden Bow      a8  UFS de Darwin   f0  inicio Linux/PA
12  Compaq diagnost 5c  Priam Edisk     a9  NetBSD          f1  SpeedStor
14  FAT16 oculta <3 61  SpeedStor       ab  arranque de Dar f4  SpeedStor
16  FAT16 oculta    63  GNU HURD o SysV af  HFS / HFS+      f2  DOS secondary
17  HPFS/NTFS ocult 64  Novell Netware  b7  BSDI fs         fb  VMFS de VMware
18  SmartSleep de A 65  Novell Netware  b8  BSDI swap       fc  VMKCORE de VMwa
1b  FAT32 de W95 oc 70  DiskSecure Mult bb  Boot Wizard hid fd  Linux raid auto
1c  FAT32 de W95 (L 75  PC/IX           bc  Acronis FAT32 L fe  LANstep
1e  FAT16 de W95 (L 80  Minix antiguo   be  arranque de Sol ff  BBT
""")

def orden_P():
	print("""Disk /dev/sda: 10 GiB, 10737418240 bytes, 20971520 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xd92da61f

Disposit.  Inicio    Start    Final Sectores  Size Id Tipo
/dev/sda1  *          2048 18874367 18872320    9G 83 Linux
/dev/sda2         18876414 20969471  2093058 1022M  5 Extendida
/dev/sda5         18876416 20969471  2093056 1022M 82 Linux swap / Solaris""")


def orden_deX():
	print("""Help (expert commands):
DOS (MBR)
b move beginning of data in a partition
i change the disk identifier

Geometria
c change number of cylinders
h change number of heads
s change number of sectors/track

Generic
p print the partition table
v verify the partition table
p print the raw data of the first sector from the device
D print the raw data of the disklabel from the device
f fix partitions order
m print this menu

Save & Exit
q quit without saving changes
r return to main menu""")


def orden_W():
	print("""
The partition table has been altered.
Calling ioctl() to re-read partition table.
Re-reading the partition table failed.: Dispositivo o recurso ocupado

The kernel still uses the old table. The new table will be used at the next reboot or after you run partprobe(8) or kpartx(8).""")

def orden_deI():
	print("""  Device: /dev/sda5
          Start: 18876416
            End: 20969471
        Sectors: 2093056
      Cylinders: 131
           Size: 1022M
             Id: 82
           Type: Linux swap / Solaris
    Start-C/H/S: 1023/63/254
      End-C/H/S: 1023/63/254""")


def orden_deN():
	print(""" All space for primary partitions is in use.
Adding logical partition 6
No free sectors available.""")
	

def orden_deT2():
	 print("""  0  Vacía           24  DOS de NEC      81  Minix / Linux a bf  Solaris
 1  FAT12           27  WinRE NTFS ocul 82  Linux swap / So c1  DRDOS/sec (FAT-
 2  XENIX root      39  Plan 9          83  Linux           c4  DRDOS/sec (FAT-
 3  XENIX usr       3c  PartitionMagic  84  OS/2 hidden or  c6  DRDOS/sec (FAT-
 4  FAT16 <32M      40  Venix 80286     85  Linux extendida c7  Syrinx
 5  Extendida       41  PPC PReP Boot   86  Conjunto de vol da  Datos sin SF
 6  FAT16           42  SFS             87  Conjunto de vol db  CP/M / CTOS / .
 7  HPFS/NTFS/exFAT 4d  QNX4.x          88  Linux plaintext de  Utilidad Dell
 8  AIX             4e  QNX4.x segunda  8e  Linux LVM       df  BootIt
 9  AIX arrancable  4f  QNX4.x tercera  93  Amoeba          e1  DOS access
 a  Gestor de arran 50  OnTrack DM      94  Amoeba BBT      e3  DOS R/O
 b  W95 FAT32       51  OnTrack DM6 Aux 9f  BSD/OS          e4  SpeedStor
 c  W95 FAT32 (LBA) 52  CP/M            a0  Hibernación de  ea  Rufus alignment
 e  W95 FAT16 (LBA) 53  OnTrack DM6 Aux a5  FreeBSD         eb  BeOS fs
 f  W95 Ext'd (LBA) 54  OnTrackDM6      a6  OpenBSD         ee  GPT
10  OPUS            55  EZ-Drive        a7  NeXTSTEP        ef  EFI (FAT-12/16/
11  FAT12 oculta    56  Golden Bow      a8  UFS de Darwin   f0  inicio Linux/PA
12  Compaq diagnost 5c  Priam Edisk     a9  NetBSD          f1  SpeedStor
14  FAT16 oculta <3 61  SpeedStor       ab  arranque de Dar f4  SpeedStor
16  FAT16 oculta    63  GNU HURD o SysV af  HFS / HFS+      f2  DOS secondary
17  HPFS/NTFS ocult 64  Novell Netware  b7  BSDI fs         fb  VMFS de VMware
18  SmartSleep de A 65  Novell Netware  b8  BSDI swap       fc  VMKCORE de VMwa
1b  FAT32 de W95 oc 70  DiskSecure Mult bb  Boot Wizard hid fd  Linux raid auto
1c  FAT32 de W95 (L 75  PC/IX           bc  Acronis FAT32 L fe  LANstep
1e  FAT16 de W95 (L 80  Minix antiguo   be  arranque de Sol ff  BBT
""")                 
	                 

def orden_s():
	print("""Created a new partition 1 of type 'Linux native' and of     size 10 GiB.
293 Created a new partition 2 of type 'Linux swap' and of size 47,1 MiB.
294 Created a new partition 3 of type 'Whole disk' and of size 10 GiB.
295 Created a new Sun disklabel.""")	                


def orden_G():
	 print("""Created a new partition 11 of type 'SGI volume' and of s    ize 10 GiB.
285 Created a new partition 9 of type 'SGI volhdr' and of size 2 MiB.
286 Created a new SGI disklabel.""")


def orden_deb():
	print("""There is no *BSD partition on /dev/sda.

The device (null) does not contain BSD disklabel.""")


def orden_deImay():
	print("""Cylinders as display units are deprecated.

Created a new DOS disklabel with disk identifier 0xd92da61f.
Created a new partition 1 of type 'OS/2 Boot Manager' and of size 9 GiB.
Created a new partition 2 of type 'Extended' and of size 1019,7 MiB.
Created a new partition 5 of type 'Linux swap / Solaris' and of size 1019,7 MiB.
Script successfully applied.""")



####
#-- Variables globales:

contador = 0
letras_menu_X = ["b", "i", "c", "h", "s", "p", "v", "d", "D", "f"]
valor_Yes = ["y", "Y", "YES", "yes", "Yes"]




###
### -- Codigo:

while True:
	try:
		orden = str(input("Orden (m para obtener ayuda): "))
		if orden == "m":
			orden_M()

		elif orden == "a":
		
			num_part = int(input("Numero de particion (1,2,5, default 5): "))
			if num_part == 1 or num_part == 2 or num_part == 5:
				if contador%2 == 0:
					print("The bootable flag on partition",  num_part,  " is enabled now.")
					contador = contador + 1
				else:
					print("The bootable flag on partition 1 is disabled now.")
					contador = contador + 1				
			else:
				print("No existe esa particion.")

		elif orden == "b": 
			orden_deb()
			orden_B = str(input("Do you want to create a BSD disklabel? [Y]es/[N]o: "))
			if  orden_B in valor_Yes:
				print("There is no *BSD partition on /dev/sda.")
	


		elif orden == "c":
			print("DOS Compatibility flag is set (DEPRECATED!)")

	

		elif orden == "d":
			orden_D = int(input("Numero de particion (1-5, default 5): "))
			if orden_D in range(0,5):
				print("La particion ", orden_D, " ha sido eliminada")
		
			elif orden_D not in range(0,5):
				print("Value out of range") 

				

		elif orden == "F":
			orden_F()
	

		elif orden == "l":
			orden_L()


		elif orden == "n":
			orden_deN()
			while True:
				orden_N = str(input("Select (default p: )"))
				if orden_N == "p":
					num_p = int(input("Numero de particion: "))
					input("First sector(63-20971519, default 63)")
					input("Last sector, +sectors or +size{K,M,G,T,P} (63-18876413, default 18876413): ")
					print("Created a new partition ", num_p, "of Type 'Linux'")
					break
	
				elif orden_N == "l":
					print("Adding logical partition")
					break
				else: 
					print("Valor no valido")
		
		elif orden == "p":
			orden_P()

	

		elif orden == "t":
			while True:
				orden_T = int(input("Numero de particion (1,2,5, default 5): "))
				if orden_T == 1 or orden_T == 2 or orden_T == 5:
					while True:
						orden_T2 = str(input("Partition type (type L to list all types): "))
						if orden_T2 == "l":
							orden_deT2()
						else:
							print("Ha selecionado ", orden_T2)
							break
					break	
				else:
					print("Orden no valida")



		elif orden == "v":
			print("Remaining 4095 unallocated 512-byte sectors.")



		elif orden == "i":
			orden_i = int(input("Numero de particion (1,2,5, default 5): "))
			if orden_i == 1 or orden_i  == 2 or orden_i == 5:
				orden_deI()
			else: 
				print(orden_i, " :unknown command")



		elif orden == "u":
			print("Changing display/entry units to cylinders (DEPRECATED!).")



		elif orden == "x": 
			while True:		
				orden_X = str(input("Orden avanzada (m para obtener ayuda, q o r para salir): "))	
				if orden_X == "m":
					orden_deX()
			
				elif orden_X == "q" or orden_X == "r":
					break
		
				elif orden_X in letras_menu_X: 
					print("Ha seleccionado ", orden_X, " ,funciona igual que menu anterior")
				else:
					print("Valor no encontrado")
				
	


		elif orden == "I":
			orden_I = str(input("Enter script file name: "))
			orden_deImay()



		elif orden == "O":
			str(input("Enter script file name: "))
			print("Script successfully saved.")
	


		elif orden == "w":
			orden_W()
			break
	


		elif orden == "q":
			break;


		elif orden == "g":
			print("Created a new GPT disklabel (GUID: 497DBC3A-7063-4200-B551-D8542DE01EBE).")



		elif orden == "G":
			orden_G()


		elif orden == "o":
			print("Created a new DOS disklabel with disk identifier 0x65ca7f9c.")



		elif orden == "s":
			orden_s()

		else:
			print(orden, ": unknown command")

# Control de excepciones:
	except ValueError:
		print("ERROR. Tipo de dato introducido no concuerda con el tipo de dato pedido por pantalla . Intentelo de nuevo.")
